import telebot
from telebot import types  # бот написан с использованием библиотеки pyTelegramBotAPI
import os
import schedule  # библиотека для создания расписаний
import time
from time import sleep
from threading import Thread
from addons.weather import weather_forecast_rn
from addons.bank import bank_usd, bank_rub, bank_eur, bank_info_msg
from addons.covid import parse_covid
from addons.news import news_rn
from addons.best_currency import for_usd, for_eur, for_rub

bot = telebot.TeleBot(os.environ['API_BOT'])

set_time_period = 6


def markup_template():  # шаблон главной Inline клавиатуры
    main_markup = types.InlineKeyboardMarkup()
    button_weather = types.InlineKeyboardButton(text='Погода', callback_data='weather')
    button_bank = types.InlineKeyboardButton(text='Курсы валют', callback_data='bank')
    button_covid = types.InlineKeyboardButton(text='Covid19', callback_data='covid')
    button_news = types.InlineKeyboardButton(text='Новости', callback_data='news')
    button_auto = types.InlineKeyboardButton(text='Рассылка сообщений', callback_data='auto')
    main_markup.add(button_weather, button_bank, button_covid, button_news, button_auto)
    return main_markup


def auto_bank(chat_id):     # функция для работы автоматической рассылки
    return bot.send_message(chat_id, for_usd()), bot.send_message(chat_id, for_eur()), \
           bot.send_message(chat_id, for_rub())


@bot.message_handler(commands=['start'])  # декоратор обработчика сообщений, действия при вызове команды /start
def gen_main_inline_markup(message):  # создание главной Inline клавиатуры
    markup_template()
    bot.send_message(message.chat.id, 'Выберите один из пунктов меню', reply_markup=markup_template())


@bot.message_handler(commands=['set_time_period'])   # команда установки своего периода рассылки
def set_period_time(message):
    msg = bot.reply_to(message, f"Период рассылки на данный момент (часы): {set_time_period}\n"
                                f"Введите новый период")
    bot.register_next_step_handler(msg, set_time_period_next_step)


def set_time_period_next_step(message):   # проверка введенных данных периода
    if message.text.isdigit():
        global set_time_period
        set_time_period = int(message.text)
        if set_time_period != 0:
            msg = bot.reply_to(message, f'Период установлен на {set_time_period}ч \n'
                                        f'Чтобы изменения вступили в силу, выключите и затем включите уведомления')
            gen_main_inline_markup(msg)
        else:
            msg = bot.reply_to(message, f'Введите положительное число не равное 0')
            bot.register_next_step_handler(msg, set_time_period_next_step)
    else:
        msg = bot.reply_to(message, f'Введите целое число')
        bot.register_next_step_handler(msg, set_time_period_next_step)


@bot.callback_query_handler(func=lambda call: True)  # декоратор обработчика обратных вызовов
def answer(call):  # фунционал клавиатуры (что происходит при нажатии каждой клавиши)
    if call.data == 'weather':
        weather_keyboard = types.InlineKeyboardMarkup(row_width=2)
        button_minsk = types.InlineKeyboardButton(text='Минск', callback_data='minsk')
        button_msk = types.InlineKeyboardButton(text='Москва', callback_data='msk')
        button_back = types.InlineKeyboardButton(text='Назад', callback_data='main')
        weather_keyboard.add(button_minsk, button_msk, button_back)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Выберите город',
                              reply_markup=weather_keyboard)

    elif call.data == 'main':
        markup_template()
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Выберите один из пунктов меню',
                              reply_markup=markup_template())

    elif call.data == 'bank':
        bank_keyboard = types.InlineKeyboardMarkup()
        button_usd = types.InlineKeyboardButton(text='USD', callback_data='usd')
        button_eur = types.InlineKeyboardButton(text='EUR', callback_data='eur')
        button_rub = types.InlineKeyboardButton(text='RUB', callback_data='rub')
        button_info = types.InlineKeyboardButton(text='Информация', callback_data='info')
        button_back = types.InlineKeyboardButton(text='Назад', callback_data='main')
        bank_keyboard.add(button_usd, button_eur, button_rub, button_back, button_info)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Выберите валюту',
                              reply_markup=bank_keyboard)

    elif call.data == 'auto':
        auto_keyboard = types.InlineKeyboardMarkup(row_width=1)
        button_bank = types.InlineKeyboardButton(text='Лучшие курсы валют', callback_data='autobank_menu')
        button_back = types.InlineKeyboardButton(text='Назад', callback_data='main')
        auto_keyboard.add(button_bank, button_back)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Выберите функцию для рассылки сообщений',
                              reply_markup=auto_keyboard)

    elif call.data == 'autobank_menu':
        autobank_menu = types.InlineKeyboardMarkup(row_width=1)
        button_period = types.InlineKeyboardButton(text='Настройки периодической рассылки',
                                                   callback_data='autobank_period')
        button_back = types.InlineKeyboardButton(text='Назад', callback_data='auto')
        autobank_menu.add(button_period, button_back)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Выберите пункт меню',
                              reply_markup=autobank_menu)

    elif call.data == 'autobank_period':
        autobank_menu_period = types.InlineKeyboardMarkup()
        button_enable = types.InlineKeyboardButton(text='Включить', callback_data='autobank_period_enable')
        button_disable = types.InlineKeyboardButton(text='Выключить', callback_data='autobank_period_disable')
        button_back = types.InlineKeyboardButton(text='Назад', callback_data='autobank_menu')
        autobank_menu_period.add(button_enable, button_disable, button_back)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Функция периодической рассылки каждые 6 часов и в 9:00 каждый день\n'
                                   'Период рассылки можно изменить\nкомандой /set_time_period введя количество часов',
                              reply_markup=autobank_menu_period)

    elif call.data == 'covid':
        bot.send_message(call.message.chat.id, parse_covid())
        gen_main_inline_markup(call.message)

    elif call.data == 'news':
        bot.send_message(call.message.chat.id, news_rn())
        gen_main_inline_markup(call.message)

    elif call.data == 'usd':
        bot.send_message(call.message.chat.id, bank_usd())
        bot.send_message(call.message.chat.id, for_usd())

    elif call.data == 'eur':
        bot.send_message(call.message.chat.id, bank_eur())
        bot.send_message(call.message.chat.id, for_eur())

    elif call.data == 'rub':
        bot.send_message(call.message.chat.id, bank_rub())
        bot.send_message(call.message.chat.id, for_rub())

    elif call.data == 'info':
        bot.send_message(call.message.chat.id, bank_info_msg())

    elif call.data == 'minsk':
        bot.send_message(call.message.chat.id, weather_forecast_rn('Minsk'))
        gen_main_inline_markup(call.message)

    elif call.data == 'msk':
        bot.send_message(call.message.chat.id, weather_forecast_rn('Moscow'))
        gen_main_inline_markup(call.message)

    elif call.data == 'autobank_period_enable' or 'autobank_period_disable':  # создание расписания
        if call.data == 'autobank_period_enable':
            schedule.every().day.at('06:00').do(auto_bank, chat_id=call.message.chat.id)
            schedule.every(int(set_time_period)).hours.at(':00').do(auto_bank, chat_id=call.message.chat.id)
        elif call.data == 'autobank_period_disable':
            schedule.clear()


def schedule_checker():  # постоянная проверка расписания действий
    while True:
        schedule.run_pending()
        sleep(1)


Thread(target=schedule_checker).start()

bot.polling(none_stop=True, interval=1)
