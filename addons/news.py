import requests
from bs4 import BeautifulSoup  # для парсинга информации используется BeautifulSoup

HOST = 'https://people.onliner.by'


def news_rn():
    url = 'https://people.onliner.by/'
    html = requests.get(url).text
    soup = BeautifulSoup(html, 'html.parser')
    all_news = soup.find_all('div', class_='news-tidings__clamping')  # находим все новости
    news_list = []
    for article in all_news[0:3]:  # собираем данные только для первых трех
        news_list.append({
            'title': article.find('span', class_='news-helpers_hide_mobile-small').get_text(),
            'main_text': article.find('div', class_='news-tidings__speech').get_text(strip=True),
            'link': HOST + article.find('a', class_='news-tidings__link').get('href')
        })

    news_final = f"Последние новости:\n\n" \
                 f"{news_list[0]['title']}\n\n{news_list[0]['main_text']}\n\n{news_list[0]['link']}\n\n" \
                 f"________________________________________________" \
                 f"\n\n{news_list[1]['title']}\n\n" \
                 f"{news_list[1]['main_text']}\n\n{news_list[1]['link']}\n\n" \
                 f"________________________________________________" \
                 f"\n\n{news_list[2]['title']}\n\n{news_list[2]['main_text']}\n\n" \
                 f"{news_list[2]['link']}\n\n\n"
    return news_final
