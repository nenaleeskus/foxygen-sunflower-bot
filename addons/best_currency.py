import requests
from bs4 import BeautifulSoup

HEADERS = {
    'Accept-Encoding': 'gzip, deflate, sdch',
    'Accept-Language': 'en-US,en;q=0.8',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive', }


def parse_currency():
    url = 'https://myfin.by/currency/minsk'
    html = requests.get(url, headers=HEADERS).text
    soup = BeautifulSoup(html, 'html.parser')
    list_curr = []
    all_curr = soup.find_all('tr', class_='tr-tb')
    for curr in all_curr:
        list_curr.append({
            'title': curr.find('td').find_next('span').find_next('a').text,
            'usd_curr_buy': float(curr.find_all('td')[1].text),
            'usd_curr_sell': float(curr.find_all('td')[2].text),
            'eur_curr_buy': float(curr.find_all('td')[3].text),
            'eur_curr_sell': float(curr.find_all('td')[4].text),
            'rub_curr_buy': float(curr.find_all('td')[5].text),
            'rub_curr_sell': float(curr.find_all('td')[6].text)
        })

    return list_curr


def for_usd():
    best_list_sell = sorted(parse_currency(), key=lambda k: k['usd_curr_sell'])
    best_list_buy = sorted(parse_currency(), key=lambda k: k['usd_curr_buy'])
    final_msg_usd_sell = ''
    final_msg_usd_buy = ''
    for i in range(4):
        final_msg_usd_sell += f"{best_list_sell[i]['title']}:  {best_list_sell[i]['usd_curr_buy']} " \
                              f" {best_list_sell[i]['usd_curr_sell']}\n"
        final_msg_usd_buy += f"{best_list_buy[i]['title']}:  {best_list_buy[i]['usd_curr_buy']} " \
                             f" {best_list_buy[i]['usd_curr_sell']}\n"
    return 'Лучшие курсы валют для покупки доллара (покупка / продажа банком):\n\n' + final_msg_usd_sell + '\n\n' \
        'Лучшие курсы валют для продажи доллара (покупка / продажа банком):\n\n' + final_msg_usd_buy


def for_eur():
    best_list_sell = sorted(parse_currency(), key=lambda k: k['eur_curr_sell'])
    best_list_buy = sorted(parse_currency(), key=lambda k: k['eur_curr_buy'])
    final_msg_eur_sell = ''
    final_msg_eur_buy = ''
    for i in range(4):
        final_msg_eur_sell += f"{best_list_sell[i]['title']}:  {best_list_sell[i]['eur_curr_buy']} " \
                              f" {best_list_sell[i]['eur_curr_sell']}\n"
        final_msg_eur_buy += f"{best_list_buy[i]['title']}:  {best_list_buy[i]['eur_curr_buy']} " \
                             f" {best_list_buy[i]['eur_curr_sell']}\n"
    return 'Лучшие курсы валют для покупки евро (покупка / продажа банком):\n\n' + final_msg_eur_sell + '\n\n' \
        'Лучшие курсы валют для продажи евро (покупка / продажа банком):\n\n' + final_msg_eur_buy


def for_rub():
    best_list_sell = sorted(parse_currency(), key=lambda k: k['rub_curr_sell'])
    best_list_buy = sorted(parse_currency(), key=lambda k: k['rub_curr_buy'])
    final_msg_rub_sell = ''
    final_msg_rub_buy = ''
    for i in range(4):
        final_msg_rub_sell += f"{best_list_sell[i]['title']}:  {best_list_sell[i]['rub_curr_buy']} " \
                              f" {best_list_sell[i]['rub_curr_sell']}\n"
        final_msg_rub_buy += f"{best_list_buy[i]['title']}:  {best_list_buy[i]['rub_curr_buy']} " \
                             f" {best_list_buy[i]['rub_curr_sell']}\n"
    return 'Лучшие курсы валют для покупки 100 российских рублей (покупка / продажа банком):\n\n' \
           + final_msg_rub_sell + '\n\n' \
           'Лучшие курсы валют для продажи 100 российских рублей (покупка / продажа банком):\n\n' + final_msg_rub_buy
