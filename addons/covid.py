import requests
from bs4 import BeautifulSoup  # для парсинга информации используется BeautifulSoup


def parse_covid():
    url = 'https://www.worldometers.info/coronavirus/country/belarus/'
    html = requests.get(url).text
    soup = BeautifulSoup(html, 'html.parser')
    data_cases = soup.find_all('div', class_="maincounter-number")[0].text
    data_deaths = soup.find_all('div', class_="maincounter-number")[1].text
    data_recovered = soup.find_all('div', class_="maincounter-number")[2].text
    last_update = soup.find('div', style="font-size:13px; color:#999; text-align:center").text
    covid_info = f"Общая статистика распостранения Covid19 в Беларуси\n\n" \
                 f"Всего случаев заражения: {data_cases}\nВсего смертей: {data_deaths}\n" \
                 f"Всего выздоровело: {data_recovered}\n" \
                 f"{last_update}\n\nИсточник: {url}"
    return covid_info
