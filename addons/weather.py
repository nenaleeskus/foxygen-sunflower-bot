import requests
import os
from datetime import datetime
import arrow  # библиотека для форматирования времени в нужный часовой пояс


def weather_forecast_rn(city):
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={os.environ['OPEN_API']}&units=metric" \
          "&lang=ru"
    source = 'https://openweathermap.org/'
    data = requests.get(url).json()  # получение json данных
    datetime_text = datetime.utcfromtimestamp(data['dt']).strftime('%Y-%m-%d %H:%M:%S')  # форматирование из Unix в UTC
    datetime_normal = arrow.get(datetime_text).to('Europe/Moscow').format()  # из UTC в МСК
    weather_final = f"Погода на данный момент:\n{datetime_normal} GMT\n\n{data['name']}, " \
                    f"{data['weather'][0]['description']}\n\n" \
                    f"Температура воздуха: {data['main']['temp']}°C, ощущается как {data['main']['feels_like']}°C\n\n" \
                    f"Атмосферное давление составляет {data['main']['pressure']} кПа\n\n" \
                    f"Влажность {data['main']['humidity']}%\n\nДанные получены благодаря {source}"
    return weather_final
