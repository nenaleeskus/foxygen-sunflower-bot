import requests


def bank_usd():  # получение и вывод данных для каждой валюты
    url = 'https://belarusbank.by/api/kursExchange?city=Минск'
    data = requests.get(url).json()
    bank_message = f"Доллар (покупка): {data[0]['USD_in']}\nДоллар (продажа): {data[0]['USD_out']}\n"
    return "Курсы валют по данным Беларусбанка\n\n" + bank_message


def bank_eur():
    url = 'https://belarusbank.by/api/kursExchange?city=Минск'
    data = requests.get(url).json()
    bank_message = f"Евро (покупка): {data[0]['EUR_in']}\nЕвро (продажа): {data[0]['EUR_out']}\n"
    return "Курсы валют по данным Беларусбанка\n\n" + bank_message


def bank_rub():
    url = 'https://belarusbank.by/api/kursExchange?city=Минск'
    data = requests.get(url).json()
    bank_message = f"100 российских рублей (покупка): {data[0]['RUB_in']}\n100 российских рублей (продажа):" \
                   f" {data[0]['RUB_out']}\n"
    return "Курсы валют по данным Беларусбанка\n\n" + bank_message


def bank_info_msg():
    info_msg = "Информация предоставлена ОАО «АСБ Беларусбанк» и порталом myfin.by" \

    return info_msg

